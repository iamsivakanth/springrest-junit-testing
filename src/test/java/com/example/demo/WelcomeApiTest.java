package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.demo.api.WelcomeApi;
import com.example.demo.model.User;
import com.example.demo.service.WelcomeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(WelcomeApi.class)
class WelcomeApiTest {

	@MockBean
	WelcomeService service;

	@Autowired
	MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@Test
	@Disabled
	void test1() throws Exception {

		when(service.getWelcomeMsg()).thenReturn("Welcome to Testing");

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/api/welcome");

		MvcResult result = mockMvc.perform(request).andReturn();

//		int status = result.getResponse().getStatus();

//		assertEquals(200, status);
		assertEquals("Welcome to Testing", result.getResponse().getContentAsString());
	}

	@Test
	void test2() throws Exception {

		User user = User.builder().userId(111).userName("Antra").build();

		String userJson = mapper.writeValueAsString(user);

		when(service.addUser(user)).thenReturn(true);

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/api/user/add")
				.contentType(MediaType.APPLICATION_JSON).content(userJson);

		MvcResult result = mockMvc.perform(request).andReturn();

		assertEquals("user is added", result.getResponse().getContentAsString());
	}
}