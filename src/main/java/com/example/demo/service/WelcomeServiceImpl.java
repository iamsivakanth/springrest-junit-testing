package com.example.demo.service;

import org.springframework.stereotype.Service;

import com.example.demo.model.User;

@Service
public class WelcomeServiceImpl implements WelcomeService {

	@Override
	public String getWelcomeMsg() {
		return "Welcome to Spring Rest";
	}

	@Override
	public boolean addUser(User user) {

		return true;
	}
}