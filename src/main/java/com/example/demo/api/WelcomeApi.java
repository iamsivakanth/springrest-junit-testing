package com.example.demo.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.service.WelcomeService;

@RestController
@RequestMapping(value = "/api")
public class WelcomeApi {

	@Autowired
	WelcomeService service;

	@GetMapping(value = "/welcome")
	public ResponseEntity<?> getWelcome() {

		return new ResponseEntity<String>(service.getWelcomeMsg(), HttpStatus.OK);
	}

	@PostMapping(value = "/user/add")
	public ResponseEntity<?> addUSer(@RequestBody User user) {

		if (service.addUser(user))
			return new ResponseEntity<String>("user is added", HttpStatus.CREATED);
		else
			return new ResponseEntity<String>("user is not added", HttpStatus.BAD_REQUEST);
	}
}